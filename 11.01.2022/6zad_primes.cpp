#include <iostream>
#include <string>

using namespace std;

int main()
{
    int output = 0, firstnum, secondnum, flag = 0;
    while (true){
        cin >> firstnum;
        if (firstnum > 1){
            break;
        } else {
            cout << "Invalid number" << endl;
        }
    }
    while (true){
        cin >> secondnum;
        if (secondnum > 1 && secondnum > firstnum && secondnum < 100000){
            break;
        } else {
            cout << "Invalid number" << endl;
        }
    }
    for (int x = firstnum+1;x<secondnum;x++){
        flag = 0;
        for (int b = 2;b<=(int)x/2;b++){
            if (x % b == 0){
                flag = 1;
                break;
            }
        }
        if (flag == 0){
            output += to_string(x).size();
        }
    }
    cout << output;
}
