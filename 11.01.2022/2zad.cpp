#include <iostream>
#include <string>

using namespace std;

int main()
{
	int p, q, flag;
	cin >> p;
	while (true){
		cin >> q;
		if (q<=p){
			cout << "Invalid number" << endl;
		} else {
			break;
		}
	}
	for (int x = p + 1;x<q;x++){
		flag = 0;
		for (int b = 2;b<=(int)x/2;b++){
			if (x%b==0){
				flag = 1;
				break;
			}
		}
		if (flag == 0){
			cout << to_string(x) << " ";
		}
	}
	cout << endl;
}