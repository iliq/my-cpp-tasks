#include <iostream>
#include <string>

using namespace std;

int main()
{
    int num, flag = 0, len = 0, times;
    cin >> times;
    for (int x = 0;x<times;x++){
        cin >> num;
        flag = 0;
        for (int b = 2;b<=(int)num/2;b++){
            if (num % b == 0){
                flag = 1;
                break;
            }
        }
        if (flag == 0){
            len += 1;
        }
    }
    cout << len;
}
