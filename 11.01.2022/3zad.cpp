#include <iostream>
#include <string>

using namespace std;

int main()
{
    int num, flag;
    string output;
    while (true){
        cin >> num;
        if (num <= 0){
            break;
        } else {
            flag = 0;
            for (int b = 2;b<=(int)num/2;b++){
                if (num % b == 0){
                    flag = 1;
                    break;
                }
            }
            if (flag == 0){
                output += to_string(num) + " ";
            }
        }
    }
    cout << output;
}
