#include <iostream>
#include <string>

using namespace std;

int main(){
    int N, a, b;
    string s, s_rev, c;
    bool br = false;
    while (true){
        cin >> N;
        if (1 < N && N <= 1000){
            break;
        } else {
            cout << "NO" << endl;
            cin.ignore();
            cin.clear();
        }
    }
    for (a = N;a>=1;a--){
        for (b = N;b>=1;b--){
            s = to_string(a*b);
            s_rev = "";
            for (int x = s.size()-1;x>=0;x--){
                c = "";
                c.push_back(s[x]);
                s_rev += c;
            }
            if (s == s_rev){
                br = true;
                break;
            }
        }
        if (br){
            break;
        }
    }
    if (a > b){
        cout << b << " " << a << endl;
    } else {
        cout << a << " " << b << endl;
    }
    cout << a*b;
    return 0;
}
