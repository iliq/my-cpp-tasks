#include <iostream>
#include <string>

using namespace std;
string multiply(string text, int by){
    string output = "";
    for (int x = 0;x<by;x++){
        output += text;
    }
    return output;
}
int main(){
    int n, k, out;
    while (true){
        cin >> n;
        if (n >= 1 && n <= 18){
            break;
        } else {
            cout << "Invalid number" << endl;
            cin.ignore();
            cin.clear();
        }
    }
    while (true){
        cin >> k;
        if (k >= 1 && k <= 10000000){
            break;
        } else {
            cout << "Invalid number" << endl;
            cin.ignore();
            cin.clear();
        }
    }
    for (int x = n*10;x<((n*100)-1);x++)){
        if (x%k==0){
            out = x;
            break;
        }
    }
    cout << out;
    return 0;
}
